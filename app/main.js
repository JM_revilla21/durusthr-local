import Vue from 'nativescript-vue'
import Timekeeping from './components/Timekeeping/Timekeeping'
import RequestForms from './components/RequestForms/RequestForms'
import ViewForm from './components/RequestForms/ViewForm'
import VueDevtools from 'nativescript-vue-devtools'
import AuditTrail from './components/Profile/AuditTrail'
import CompanyProfile from './components/Profile/CompanyProfile'
import company_profile_view from './components/Profile/company_profile_view'
import sample from './components/Document'
import personnel_information from './components/Profile/personnel_information'
import Profile from './components/Profile/Profile'
import signatory_details from './components/Profile/signatory_details'
import my_money_fund_transfer from './components/Payslip/PayslipForm'
import Login from './components/Login'
import Dashboard from './components/Dashboard'
import crt from './components/CRT_Onboarding.vue'
import crtv2 from './components/CRT_Onboardingv2.vue'
import schedule_Request from './components/Timekeeping/Schedule_Request'

if(TNS_ENV !== 'production') {
  Vue.use(VueDevtools)
}
  
// Prints Vue logs when --env.production is *NOT* set while building

import VueCurrencyFilter from 'vue-currency-filter'


Vue.use(VueCurrencyFilter,
  {
    symbol : '₱',
    thousandsSeparator: ',',
    fractionCount: 2,
    fractionSeparator: '.',
    symbolPosition: 'front',
    symbolSpacing: true
  })

Vue.registerElement('VueCurrencyFilter', ()=> require('vue-currency-filter').VueCurrencyFilterFab);
Vue.registerElement('MaskedTextField', () => require('nativescript-masked-text-field').MaskedTextField);
// Vue.registerElement('Fab', ()=> require('nativescript-floatingactionbutton').Fab)

Vue.config.silent = (TNS_ENV === 'production')

Vue.registerElement('InputMask', () => require('nativescript-input-mask').InputMask);

Vue.registerElement('CheckBox', () => require('@nstudio/nativescript-checkbox').CheckBox);
// Vue.registerElement('CheckBox', () => require('nativescript-checkbox').CheckBox)
Vue.registerElement('DrawingPad',() => require('nativescript-drawingpad').DrawingPad);
Vue.registerElement('Gif',() => require('nativescript-gif').Gif);
// Vue.registerElement('PullToRefresh', () => require('@nstudio/nativescript-pulltorefresh').PullToRefresh)
Vue.registerElement('PullToRefresh', () => require('@nstudio/nativescript-pulltorefresh').PullToRefresh);
Vue.registerElement('Calendar', () => require('nativescript-ui-calendar').RadCalendar)
Vue.registerElement('PDFView', () => require('nativescript-pdf-view').PDFView);
require( "nativescript-localstorage" );
require( "nativescript-orientation" );
require( "nativescript-globalevents" );
// require("nativescript-plugin-firebase");
localStorage.setItem('URL','https://ws.durusthr.com/ILM_WS_Live/')
// localStorage.setItem('ClientName','ilmdev')
// localStorage.setItem('EmpID','i034')

Vue.registerElement('Toast', () => require('nativescript-toast').Toast)

new Vue({
  
  render: h => h('frame', [h(Login)])
}).$start()
